<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('home', 'HomeController@home');
Route::get('back', 'HomeController@back')->name('back');
Route::get('welcome/{code}', 'HomeController@blog');

//ruta para volver a la página principal del panel de control del back
Route::get('/back', 'NewsController@back');
//rutas para la página de news (crear la noticia)
Route::get('/createnews', 'NewsController@index');
Route::get('/createnews', 'NewsController@showCategories');
Route::post('createnews', 'NewsController@store');
//rutas para la página de traducciones de news (insertar noticia)
Route::get('transNews', 'TransNewsController@index');
Route::post('transNews', 'TransNewsController@store');
Route::get('transNews', 'TransNewsController@showId');
//rutas para la página de categories (crear la categoria)
Route::get('/createcategories', 'CategoriesController@index');
Route::post('createcategories', 'CategoriesController@store');
//rutas para la página de traducciones de categorias (insertar Categorias)
Route::get('/transCategories', 'TransCategoriesController@index');
Route::post('transCategories', 'TransCategoriesController@store');
Route::get('transCategories', 'TransCategoriesController@showId');
//rutas para la página de lenguages (insertar lenguas)
Route::get('/createlanguages', 'LanguagesController@index');
Route::post('createlanguages', 'LanguagesController@store');
//rutas para la página general del blog
Route::get('welcome/{code}', 'NewsController@show');
//rutas para la página general del blog con un idioma en concreto
Route::get('welcome/{code}', 'NewsController@idioma');
//rutas para la página individual de la noticia
Route::get('new/{titleSeo}/{code}', 'NewsController@showNew');

Route::post('welcome/{code}', 'NewsController@filtro');
