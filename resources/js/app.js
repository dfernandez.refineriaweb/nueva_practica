require('./bootstrap');

$(document).ready(function(){
    $("#cat").submit(function(event){
        event.preventDefault();
        var category = $("#category").val();
        var url = window.location.toString();
        // var fullUrl = url.substr(0,url.length-2);
        $.ajax({
            headers:{'X-CSRF-TOKEN': $('input[name="_token"]').val()},
            data: {'category': category},
            url: url,
            dataType: 'json',
            type: 'post',
            success: function (response){
                console.log(response);
                var tabla = "";

                for(var x = 0;x < response.length;x++) {
                    var news = response[x].news;
                    var lang = response[x].lang;

                    for(var i = 0;i < news.length;i++) {
                        // alert(news[i].id);
                        // alert(news[i]['trans_news'][0].titleSeo);
                        tabla +='<div class="col-lg-4">';
                        tabla +='<img class="bd-placeholder-img rounded-rectangular" width="100%" height="200" src="'+news[i].image+'">';
                        tabla +='<h2>'+news[i].dateUp+'</h2>';
                        tabla +='<p>'+news[i]['trans_news'][0].title+'</p>';
                        if(url.substr(-2) == "es")
                        {
                            tabla +='<p><a class="btn btn-secondary" href="../new/'+news[i]['trans_news'][0].titleSeo+'/'+lang+'" role="button">Seguir Leyendo &raquo;</a></p>';
                        }
                        if(url.substr(-2) == "en")
                        {
                            tabla +='<p><a class="btn btn-secondary" href="../new/'+news[i]['trans_news'][0].titleSeo+'/'+lang+'" role="button">Keep reading &raquo;</a></p>';
                        }
                        tabla +="</div>";
                    }
                $('#respuesta').html(tabla);
                }
            }
        });
    });
});
