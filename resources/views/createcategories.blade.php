@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header" ><h3 style="color:black;" >Create Category</h3></div>
                    <div class="card-body">
            @if ($errors->any())
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form method="post" action="">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="date_up" style="color:black;"  >Date Up</label>
                <div class="col-sm-4">
                    <input class="form-control" type="date" id="date_up" name="date_up" value="{{ old('dateUp') }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <input  class="btn btn-primary" type="submit" value="Save">
                </div>
            </div>
            <div id="link" class="alert alert-dark" role="alert">
                <a class="alert-link" href="transCategories">Go to Translation Categories</a>
            </div>
            <div id="link" class="alert alert-dark" role="alert">
                <a class="alert-link" href="back">Principal Page</a>
            </div>
        </form>
   </div>
            </div>
        </div>
    </div>
</div>


@endsection
