@extends('layouts.app')

@section('content')
{{-- @include('header') --}}
{{-- {{ dd($esp) }} --}}
<main role="main" class="inner cover">
    <h1 class="cover-heading">You are logged in!</h1>
    <p class="lead"></p>
    <p class="lead"><a href="welcome/{{ $esp->first()->code }}" class="btn btn-lg btn-secondary">Ver Blog</a></p>
    <p class="lead"><a href="back" class="btn btn-lg btn-secondary">BackEnd</a></p>
    </main>
{{-- @include('footer') --}}
@endsection
