@extends('layouts.blog')
@section('content')
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        @foreach($categories as $category)
            <li data-target="#myCarousel" data-slide-to="{{ $category->id }}"></li>
        @endforeach
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <svg class="bd-placeholder-img" width="100%" height="300px" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#777"/></svg>
        <div class="container">
          <div class="carousel-caption text-left">
            @if(substr(Request::fullUrl(), -2) == "es")
                <h1>Blog de Noticias.</h1>
                <p>Desarrollado en Laravel para Refinería Web.</p>
            @elseif(substr(Request::fullUrl(), -2) == "en")
                <h1>News Blog.</h1>
                <p>Developed in Laravel for Web Refinery.</p>
            @endif
            {{-- <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> --}}
          </div>
        </div>
      </div>
      @foreach($categories as $category)
            <div class="carousel-item">
                <svg class="bd-placeholder-img" width="100%" height="300px" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#777"/></svg>
                <div class="container">
                    <div class="carousel-caption">
                        <h1>{{ $category->title }}</h1>
                        {{-- <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> --}}
                        {{-- <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> --}}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->
  <div class="container marketing" style="padding-top: 5%;">
    <!-- Three columns of text below the carousel -->
    <div id="respuesta" class="row">
        @foreach($news as $new)
          <div class="col-lg-4">
            {{-- <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140" ><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text></svg> --}}
            <img class="bd-placeholder-img rounded-rectangular" width="100%" height="200" src="{{ $new->image }}">
            <h2>{{ $new->dateUp }}</h2>
            <p>{{ $new->TransNews->first()->title }}</p>
            @if(substr(Request::fullUrl(), -2) == "es")
                <p><a class="btn btn-secondary" href="{{ url('new') }}/{{ $new->TransNews->first()->titleSeo }}/{{ $esp->first()->code }}" role="button">Seguir Leyendo &raquo;</a></p>
            @elseif(substr(Request::fullUrl(), -2) == "en")
                <p><a class="btn btn-secondary" href="{{ url('new') }}/{{ $new->TransNews->first()->titleSeo }}/{{ $eng->first()->code }}" role="button">Keep reading &raquo;</a></p>
            @endif
          </div><!-- /.col-lg-4 -->
        @endforeach
    </div><!-- /.row -->

  </div><!-- /.container -->
@endsection

