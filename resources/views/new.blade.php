{{-- {{ dd($new->TransNews->first()->title) }} --}}
@extends('layouts.blog')
@section('content')
    <div class="container marketing" style="padding-top: 5%;" >
        <hr class="featurette-divider">
        <div class="row featurette">
          <div class="col-md-7">
            <h2 class="featurette-heading">{{ $new->TransNews->first()->title }}<span class="text-muted"><br/>{{ $new->dateUp }}</span></h2>
            <p class="lead">{{ $new->TransNews->first()->description }}</p>
          </div>
          <div class="col-md-5">
            <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="500x500" style="width: 100%; height: 100%;" src="{{ $new->image}}" data-holder-rendered="true">
          </div>
          <div class="container">
            @if(substr(Request::fullUrl(), -2) == "es")
                <p class="float-right"><a href="{{ url('welcome') }}/{{ $esp->first()->code }}">Atrás</a></p>
            @elseif(substr(Request::fullUrl(), -2) == "en")
                <p class="float-right"><a href="{{ url('welcome') }}/{{ $eng->first()->code }}">Go to back</a></p>
            @endif
          </div>
        </div>

        <hr class="featurette-divider">
        <hr class="featurette-divider">
        <hr class="featurette-divider">
      </div>
@endsection
