@extends('layouts.app')
@section('content')
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header" ><h3 style="color:black;" >Create Translation News</h3></div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form method="post" action="">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="title" style="color:black;" >Title</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" id="title" name="title" value="{{ old('title') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="titleSeo" style="color:black;" >TitleSeo</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" id="titleSeo" name="titleSeo" value="{{ old('titleSeo') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="description" style="color:black;" >Description</label>
                            <div class="col-sm-12">
                                <textarea class="form-control" name="description" id="description" value="{{ old('description') }}" ></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <!--<label class="col-sm-2 col-form-label" for="id">News ID</label>-->
                           <select name="news_id" class="custom-select" id="id">
                                <option selected>Select New ID</option>
                                    @foreach($newIds as $newId)
                                        <option value="{{ $newId->id}}">{{ $newId->id}}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <!--<label class="col-sm-2 col-form-label" for="lang_id">Language ID</label>-->
                            <select name="lang_id" class="custom-select" id="id">
                                <option selected>Select Language ID</option>
                                    @foreach($langIds as $langId)
                                        <option value="{{ $langId->id}}">{{ $langId->code}}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <input class="btn btn-primary" type="submit" value="Save">
                            </div>
                        </div>
                        <div id="link" class="alert alert-dark" role="alert">
                            <a class="alert-link" href="createnews">Go to News</a>
                        </div>
                        <div id="link" class="alert alert-dark" role="alert">
                            <a class="alert-link" href="back">Principal Page</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
