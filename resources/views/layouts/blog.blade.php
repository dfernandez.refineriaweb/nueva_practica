<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title></title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/carousel/">
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
  </head>
  <body>
    <header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          @if(substr(Request::fullUrl(), -10) == "welcome/es")
            <a class="nav-link" href="{{ url('welcome') }}/{{$esp->first()->code}}" style="display: inline" ><img src="https://cdn4.iconfinder.com/data/icons/dooffy_design_flags/512/dooffy_design_icons_EU_flags_Spain.png" style="width:3%"></a>
            <a class="nav-link" href="{{ url('welcome') }}/{{$eng->first()->code}}"style="display: inline"><img src="https://cdn4.iconfinder.com/data/icons/dooffy_design_flags/512/dooffy_design_icons_EU_flags_United_Kingdom.png" style="width:3%"></a>
          @elseif(substr(Request::fullUrl(), -10) == "welcome/en")
            <a class="nav-link" href="{{ url('welcome') }}/{{$esp->first()->code}}" style="display: inline" ><img src="https://cdn4.iconfinder.com/data/icons/dooffy_design_flags/512/dooffy_design_icons_EU_flags_Spain.png" style="width:3%"></a>
            <a class="nav-link" href="{{ url('welcome') }}/{{$eng->first()->code}}"style="display: inline"><img src="https://cdn4.iconfinder.com/data/icons/dooffy_design_flags/512/dooffy_design_icons_EU_flags_United_Kingdom.png" style="width:3%"></a>
          @endif
        </li>
      </ul>
      @if(substr(Request::fullUrl(), -10) == "welcome/es")
          <form id="cat" class="form-inline mt-2 mt-md-0" action="" method="POST" >
            {{ csrf_field() }}
            <select name="category" id="category" class="form-control mr-sm-2"  >
                <option selected>Selecciona Categoria</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->title }}">{{ $category->title }}</option>
                    @endforeach
            </select>
        {{-- <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"> --}}
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
      </form>
      @elseif(substr(Request::fullUrl(), -10) == "welcome/en")
        <form  id="cat" class="form-inline mt-2 mt-md-0" action="" method="POST" >
            {{ csrf_field() }}
            <select name="category" id="category" class="form-control mr-sm-2"  >
                <option selected>Select Category</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->title }}">{{ $category->title }}</option>
                    @endforeach
            </select>
        {{-- <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"> --}}
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
      @endif
    </div>
  </nav>
</header>
  <main role="main">
    @yield('content')
  </main>
  <!-- FOOTER -->
  <footer class="container">
    @if(substr(Request::fullUrl(), -2) == "es")
        <p class="float-right"><a href="{{ url('home') }}">Página Principal</a></p>
        <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacidad</a> &middot; <a href="#">Terminos</a></p>
    @elseif(substr(Request::fullUrl(), -2) == "en")
        <p class="float-right"><a href="{{ url('home') }}">Homepage</a></p>
        <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
    @endif
  </footer>
</main>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
