@extends('layouts.app')
@section('content')
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header" ><h3 style="color:black;" >Create Language</h3></div>
                    <div class="card-body">
            @if ($errors->any())
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form method="post" action="">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="name" style="color:black;"  >Name</label>
                <div class="col-sm-5">
                    <input class="form-control" type="text" id="name" name="name" value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="code" style="color:black;"  >Code</label>
                <div class="col-sm-5">
                    <input class="form-control" type="text" id="code" name="code" value="{{ old('code') }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                <input class="btn btn-primary" type="submit" value="Save">
            </div>
        </div>
        <div id="link" class="alert alert-dark" role="alert">
            <a class="alert-link" href="back">Principal Page</a>
        </div>
        </form>
   </div>
            </div>
        </div>
    </div>
</div>
@endsection
