{{-- @include('header') --}}
@extends('layouts.app')
@section('content')
    <div class="list-group">
        <h1 class="cover-heading">Panel de control del Blog</h1>
        <a href="createnews" class="list-group-item list-group-item-action list-group-item-primary">News</a>
        <a href="createcategories" class="list-group-item list-group-item-action list-group-item-warrnig">Categories</a>
        <a href="createlanguages" class="list-group-item list-group-item-action list-group-item-success">Languages</a>
        <a href="home" class="list-group-item list-group-item-action list-group-item-danger">Go Back</a>
    </div>
@endsection
{{-- @include('footer') --}}
