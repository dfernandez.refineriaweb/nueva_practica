{{-- @include('header') --}}
@extends('layouts.app')
@section('content')
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header" ><h3 style="color:black;">Create News</h3></div>
                    <div class="card-body">
            @if ($errors->any())
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form  method="post" action="">
            {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="image" style="color:black;" >Image</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" id="image" name="image" value="{{ old('image') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label" for="dateUp"style="color:black;">Date Up</label>
                    <div class="col-sm-5">
                        <input class="form-control" type="date" id="dateUp" name="dateUp" value="{{ old('dateUp') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <!--<label class="col-lg-2 control-label" for="category_id">Category Id</label>-->
                    <select name="category_id" class="custom-select" id="id">
                        <option selected>Select Category ID</option>
                        @foreach($categoryIds as $categoryId)
                            <option value="{{ $categoryId->id}}">{{ $categoryId->id}}</option>
                    @endforeach
                    </select>
                </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <input  class="btn btn-primary" type="submit" value="Save">
                </div>
            </div>
            <div id="link" class="alert alert-dark" role="alert">
                <a class="alert-link" href="transNews">Go to Translation News</a>
            </div>
            <div id="link" class="alert alert-dark" role="alert">
                <a class="alert-link" href="back">Principal Page</a>
            </div>
        </form>
   </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- @include('footer') --}}
