<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_news', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('title', 50);
            $table->string('titleSeo', 50);
            $table->string('description', 500);

             //foreign Key
            $table->integer('news_id')->unsigned();
            $table->foreign('news_id')->references('id')->on('news');
             //foreign Key
            $table->integer('lang_id')->unsigned();
            $table->foreign('lang_id')->references('id')->on('languages');
            //primary key
            $table->primary(array('news_id', 'lang_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_news');
    }
}
