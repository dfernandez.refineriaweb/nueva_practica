<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('title', 50);

             //foreign Key
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
             //foreign Key
            $table->integer('lang_id')->unsigned();
            $table->foreign('lang_id')->references('id')->on('languages');
            //primary key
            $table->primary(array('category_id', 'lang_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_categories');
    }
}
