<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransNews extends Model
{
    protected $table= 'trans_news';
    //campos editables de la tabla categories
    protected $fillable = ['title','titleSeo','description','news_id','lang_id'];
}
