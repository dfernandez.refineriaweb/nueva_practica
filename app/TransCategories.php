<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransCategories extends Model
{
    protected $table= 'trans_categories';
    //campos editables de la tabla categories
    protected $fillable = ['title','category_id','lang_id'];
}
