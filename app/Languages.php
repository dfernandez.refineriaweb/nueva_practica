<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    protected $table= 'languages';
    //campos editables de la tabla categories
    protected $fillable = ['name','code'];
}
