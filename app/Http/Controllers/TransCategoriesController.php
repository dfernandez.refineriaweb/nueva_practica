<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransCategories;
use App\Categories;
use App\Languages;

class TransCategoriesController extends Controller
{
    public function index()
    {
        return view('/transcategories');
    }
    public function store(Request $request)
    {
        $validate = request()->validate([
            'title' => 'required|unique:trans_categories|max:50',
            'category_id' => 'required|max:10',
            'lang_id' => 'required|max:10'
        ]);
        TransCategories::create($validate);
        return redirect('transCategories');
    }
    public function showId()
    {
        $langIds = Languages::All();
        $categoryIds = Categories::All();
        return view('transCategories', compact('categoryIds', 'langIds'));
    }
}
