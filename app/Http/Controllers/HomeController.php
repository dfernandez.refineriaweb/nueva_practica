<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Languages;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        $esp = Languages::where('id', 1)->get();
        $eng = Languages::where('id', 2)->get();
        return view('home', compact('esp', 'eng'));
    }
    public function back()
    {
        return view('back');
    }
    public function blog()
    {
        return view('welcome');
    }
}
