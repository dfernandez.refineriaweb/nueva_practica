<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Languages;

class LanguagesController extends Controller
{
    public function index()
    {
        return view('/createlanguages');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:languages|max:20',
            'code' => 'required|unique:languages|max:3',
        ]);
        Languages::create($request->all());
        return redirect('createlanguages');
    }
}
