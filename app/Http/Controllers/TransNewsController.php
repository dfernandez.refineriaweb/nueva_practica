<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransNews;
use App\Languages;
use App\News;

class TransNewsController extends Controller
{
    public function index()
    {
        return view('transNews');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:trans_news|max:100',
            'titleSeo' => 'required|unique:trans_news|max:100',
            'description' => 'required|unique:trans_news|max:1000',
            'news_id' => 'required',
            'lang_id' => 'required',
        ]);
        TransNews::create($request->all());
        return redirect('transNews');
    }
    public function showId()
    {
        $langIds = Languages::All();
        $newIds = News::All();
        return view('transNews', compact('newIds', 'langIds'));
    }
}
