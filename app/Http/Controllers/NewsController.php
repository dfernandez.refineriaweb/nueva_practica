<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Categories;
use App\TransCategories;
use App\TransNews;
use App\Languages;

class NewsController extends Controller
{
    public $idioma="";
    public $category="";
    public $id="";

    public function index()
    {
        return view('/createnews');
    }
    public function back()
    {
        return view('/back');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|max:500',
            'dateUp' => 'required',
            'category_id' => 'required|max:10',
        ]);
        News::create($request->all());
        return redirect('createnews');
    }
    public function show()
    {
        $news = News::with(['TransNews' => function ($query) {
            $query->where('lang_id', 1);
            // $query->where('code', "ESP");
        }])->get();

        $categories = TransCategories::where('lang_id', 1)->get();
        $esp = Languages::where('id', 1)->get();
        $eng = Languages::where('id', 2)->get();
        // dd($esp);
        return view('welcome', compact('news', 'categories', 'esp', 'eng'));
    }
    public function showNew($titleSeo, $code)
    {
        $this->idioma = Languages::where('code', $code)->get()->first()->id;
        $this->id = TransNews::where('titleSeo', $titleSeo)->get()->first()->news_id;
        // dd($this->id);
        $new = News::with(['TransNews' => function ($query) {
            $query->where('lang_id', $this->idioma);
        }])->find($this->id);
        // dd($new);
        $categories = TransCategories::where('lang_id', 1)->get();
        $esp = Languages::where('id', 1)->get();
        $eng = Languages::where('id', 2)->get();

        return view('new', compact('new', 'esp', 'categories', 'eng'));
    }
    public function showCategories()
    {
        //buscamos las categorias que esten dadas de alta en la base de datos
        $categoryIds = Categories::ALL();
        return view('/createnews', compact('categoryIds'));
    }
    public function idioma($code)
    {
        // $this->idioma = $id;
        $this->idioma  = languages::where('code', $code)->get()->first()->id;
        // dd($esp);
        $news = news::with(['TransNews' => function ($query) {
            $query->where('lang_id', $this->idioma);
        }])->get();
        // dd($news);
        $categories = transCategories::where('lang_id', $this->idioma)->get();
        $esp = languages::where('id', 1)->get();
        $eng = languages::where('id', 2)->get();

        return view('welcome', compact('news', 'categories', 'esp', 'eng'));
    }
    public function filtro(Request $request)
    {
        $this->category = $request->category;
        // dd($this->category);

        $this->idioma = TransCategories::where('title', $this->category)->first()->lang_id;
        $this->id = TransCategories::where('title', $this->category)->first()->category_id;

        $news = News::with(['TransNews' => function ($query) {
            $query->where('lang_id', $this->idioma);
        }])->where('category_id', $this->id)->get();

        $lang = Languages::where('id', $this->idioma)->first()->code;

        $respuesta[] = ['news' => $news, 'lang' => $lang];
        // dd($respuesta);
        return json_encode($respuesta);
    }
}
