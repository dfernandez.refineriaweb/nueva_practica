<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;

class CategoriesController extends Controller
{
    public function index()
    {
        return view('/createcategories');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'date_up' => 'required',
        ]);
        Categories::create($request->all());
        return redirect('/createcategories');
    }
}
