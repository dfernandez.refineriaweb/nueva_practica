<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table= 'categories';
    //campos editables de la tabla categories
    protected $fillable = ['date_up'];
}
