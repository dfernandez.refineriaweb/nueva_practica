<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table= 'news';
    //campos editables de la tabla categories
    protected $fillable = ['image','dateUp','category_id'];

    //enlazamos la tabla de noticias con las de traducción noticias
    public function TransNews()
    {
        return $this->hasMany(TransNews::class, 'news_id', 'id');
    }
}
